//
//  DetailViewController.swift
//  ParseTest
//
//  Created by Zachary Cole on 3/3/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController {

    var names = []
    var tv = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tv = UITableView.init(frame: CGRectMake(0, 0, view.bounds.size.width, view.bounds.size.height), style: UITableViewStyle.Plain)
        
        tv.dataSource = self
        tv.delegate = self
        tv.registerClass(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
        self.view.addSubview(tv)
        
        get()
        
        tv.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        dispatch_async(dispatch_get_main_queue()) {
            self.get()
            self.tv.reloadData()
        }
        
    }
    
    func get() {
        let url:NSURL = NSURL(string: "https://zacharycolefirst.herokuapp.com/parse/classes/Game")!
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.cachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("First", forHTTPHeaderField: "X-Parse-Application-ID")
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            //populate array
            //            let responseString = NSString.init(data: data!, encoding: NSUTF8StringEncoding)
            //reload data
            do {
                var json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? Dictionary <String, AnyObject>
                self.names = (json!["results"] as? Array<AnyObject>)!
                dispatch_async(dispatch_get_main_queue()) {
                    self.tv.reloadData()
                }
            }
            catch {
                
            }
            NSLog("test")
        }
        
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return names.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)
        
        let d : NSDictionary =  names[indexPath.row] as! NSDictionary
        
        var s = (d["name"] as? String)! + ", "
        s.appendContentsOf( (d["color"] as? String)!)
        cell.textLabel?.text = s
        
        cell.detailTextLabel?.text = d["color"] as? String
        
        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
